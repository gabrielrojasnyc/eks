provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudwatch_log_group" "yoda" {
  name = "Yada"

  tags = {
    Environment = "production"
    Application = "serviceA"
  }
}